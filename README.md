# BBB set IP address

----

Unpublished work © 2015, The Narwhal Group

All Rights Reserved

THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF THE NARWHAL GROUP.

The copyright notice above does not evidence any
actual or intended publication of such source code.

----

## Overview

This application is a simply utility that can be used to set the IP address for the `eth0` interface on a [BeagleBone Black](http://beagleboard.org/BLACK). It maintains a settings file and then uses that to create a `/etc/network/interfaces` file. Its usage is:

    narwhal@narwhalfbs:~$ set_ip --help
    set_ip [OPTION]
    where OPTION is:
        -i, --interactive  enter interactive session
        -r, --restore      restores previous settings
        -f, --factory      restores factory settings
        -g, --generate     generates new interfaces file from settings
        -s, --show         show current ethernet settings

    if no OPTION is passed, enter interactive session.

You must run the command as superuser. The options have the following meanings:

- `interactive` option will simply show the menu system described below. This is the default if no command line parameters are passed to the command.
- `restore` option will copy, if it exists, `/etc/network/interfaces.bak` to `/etc/network/interfaces`.
- `factory` option will copy `/etc/narwhal.d/interfaces.factory` to `/etc/network/interfaces` and delete `/etc/network/interfaces.bak` if it exists.
- `generate` option will use the settings in `/etc/narwhal.d/interfaces.conf` and the template file `/etc/narwhal.d/interfaces.template` to create `/etc/network.interfaces` file. Prior to creating the new file a backup copy of the original will be made.
- `show` option will display the current ethernet and NTP settings the device is using.

Most of the time, you will simply enter the command and be presented with a menu-driven application capable of configuring the network of the board.

If the IP address configuration is actually changed by the command, `ifdown` and `ifup` are called to attempt to change the IP address of the host. If this fails for some reason, it is recommended that the host be rebooted to attempt to set the new IP address.

## Setting the IP address

The `interactive` mode, displays a menu and allows a user to walk through various configuration settings:

    narwhal@narwhalfbs:~$ sudo set_ip
    Configure Flashing Beacon System
        1) Set IP address
        2) Configure NTP server(s)
        3) Quit

If you select `1`, it will walk you through setting IP addresses for network, ip address, netmask and ip gateway. When asking for input, it will show the previous setting for each of the entries as part of the prompt:

	narwhal@narwhalfbs:~$ sudo set_ip
	Configure Flashing Beacon System
	    1) Set IP address
	    2) Configure NTP server(s)
	    3) Quit
	1
	Should we get IP address from DHCP [y/N]?
	Enter IP network ():

If you entered `y` for DHCP, it will restore the factory settings. Assuming you skipped setting it up for DHCP, the first time through entries are shown as blank. Subsequently, they will not be:

	narwhal@narwhalfbs:~$ sudo set_ip
	Configure Flashing Beacon System
	    1) Set IP address
	    2) Configure NTP server(s)
	    3) Quit
	1
	IP network: 12.1.1.0
	IP address: 12.1.1.40
	IP netmask: 255.255.255.0
	IP gateway: 12.1.1.1
	Do you want to change these settings [Y/n]?
	Should we get IP address from DHCP [y/N]?
	Enter IP network (12.1.1.0):

If you simply hit return for any of these prompts, it will keep the existing value. Continuing from above:

	Enter IP network (12.1.1.0):
	Enter IP address (12.1.1.40): 12.1.1.38
	Enter netmask (255.255.255.0):
	Enter gateway IP address (12.1.1.1):
	Configure Flashing Beacon System
	    1) Set IP address
	    2) Configure NTP server(s)
	    3) Quit
	q
	narwhal@narwhalfbs:~$ grep -A 4 "eth0" /etc/network/interfaces
	auto eth0
	iface eth0 inet static
	    address 12.1.1.38
	    netmask 255.255.255.0
	    network 12.1.1.0
	    gateway 12.1.1.1
	narwhal@narwhalfbs:~$

If you successfully set the IP address, the `/etc/narwhal.d/interfaces.conf` file is updated to reflect the changes and a new `/etc/network/interfaces` file is created.

## Configuring NTP servers

At startup, the time is set on the board using configured NTP server(s). If the unit is connected to the Internet, this setting should not need to be changed as it is configured to use well-known, pooled NTP servers. However, if the unit is on a network without Internet access, the NTP configuration should be updated to point to servers that are accessible from its network.

Start `set_ip` and request to configure NTP servers:

	narwhal@narwhalfbs:~$ sudo set_ip
	Configure Flashing Beacon System
	    1) Set IP address
	    2) Configure NTP server(s)
	    3) Quit
	    2
        NTP servers: 0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
        Do you want to change these settings [Y/n]?
        Enter NTP server(s) separated by spaces (0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org): 192.168.1.100
        Updating /etc/default/ntpdate...
	Configure Flashing Beacon System
	    1) Set IP address
	    2) Configure NTP server(s)
	    3) Quit

To set the time using the configured NTP servers, run the command:

    narwhal@narwhal:~$ sudo ntpdate-debian
    11 Aug 20:42:53 ntpdate[6089]: step time server 132.163.4.101 offset -2.938451 sec
    narwhal@narwhal:~$
