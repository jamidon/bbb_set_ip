#!/bin/sh
#
# Copyright (c) 2015 The Narwhal Group, All Rights Reserved
#
# Any modification without prior authorization from the copyright holders
# is strictly prohibited.

set -e

INSTALL="install -v -p"
INSTALL_ROOT="${INSTALL} --owner=root --group=root"
SETTINGS_FILE=/etc/narwhal.d/interfaces.conf
TEMPLATE_FILE=/etc/narwhal.d/interfaces.template
INTERFACES_FACTORY=/etc/narwhal.d/interfaces.factory
INTERFACES_FILE=/etc/network/interfaces
NARWHAL_DEVICE_SETTINGS_FILE=/etc/narwhal.d/device_name

if [ $(id -u) != "0" ]; then
    echo "Must run this script as root!"
    exit 1
fi

rm -f ${NARWHAL_DEVICE_SETTINGS_FILE}
echo Enter Narwhal device this script will be used:
read NARWHAL_DEVICE

echo Installing...
mkdir -p `dirname $SETTINGS_FILE`
${INSTALL_ROOT} --mode=644 `basename $SETTINGS_FILE` $SETTINGS_FILE
${INSTALL_ROOT} --mode=644 `basename $TEMPLATE_FILE` $TEMPLATE_FILE
${INSTALL_ROOT} --mode=644 `basename $INTERFACES_FACTORY` $INTERFACES_FACTORY
${INSTALL_ROOT} --mode=755 set_ip.sh /usr/local/bin/set_ip
echo ${NARWHAL_DEVICE} > ${NARWHAL_DEVICE_SETTINGS_FILE}
chmod 644 ${NARWHAL_DEVICE_SETTINGS_FILE}

echo Done.
