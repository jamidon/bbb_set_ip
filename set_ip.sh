#!/bin/sh
#
# Copyright (c) 2015 The Narwhal Group, All Rights Reserved
#
# Any modification without prior authorization from the copyright holders
# is strictly prohibited.

set -e

APP_NAME=`basename $0`
NARWHAL_DEVICE=`cat /etc/narwhal.d/device_name`
SETTINGS_FILE=/etc/narwhal.d/interfaces.conf
NTP_SETTINGS_FILE=/etc/default/ntpdate
TEMPLATE_FILE=/etc/narwhal.d/interfaces.template
INTERFACES_FACTORY=/etc/narwhal.d/interfaces.factory
INTERFACES_FILE=/etc/network/interfaces
INTERFACES_BACKUP="$INTERFACES_FILE.bak"

show_help () {
    echo "$APP_NAME [OPTION]"
    cat <<EOF
where OPTION is:
   -i, --interactive  enter interactive session
   -r, --restore      restores previous settings
   -f, --factory      restores factory settings
   -g, --generate     generates new interfaces file from settings
   -e, --expert       updates the interface from parameters passed on command line
   -s, --show         show current ethernet settings

if no OPTION is passed, enter interactive session.

for --expert mode, the parameters expected are:
   [ip address] [ip gateway] [net mask]

$ $APP_NAME --expert 192.168.1.2 192.168.1.1 255.255.255.0

Network must be restarted manually after using expert mode:
$ sudo systemctl restart networking

EOF
    exit $1
}

show_menu() {
    cat <<EOF
Configure Narwhal ${NARWHAL_DEVICE}
    1) Set IP address
    2) Configure NTP server(s)
    3) Quit
EOF
}

show_current_ip_settings() {
    if [ "x${IP_ADDR}" != "x" -a "x${IP_NETMASK}" != "x" -a "x${IP_GATEWAY}" != "x" ];
    then
        echo "IP address: $IP_ADDR" >&2
        echo "IP netmask: $IP_NETMASK" >&2
        echo "IP gateway: $IP_GATEWAY" >&2

        return 0
    fi

    return 1
}

show_current_ntp_settings() {
    if [ "x${NTPSERVERS}" != "x" ];
    then
        echo "NTP servers: $NTPSERVERS" >&2

        return 0
    fi

    return 1
}

ip_address_has_changed() {
    if [ "x${1}" = "x${IP_ADDR}" -a "x${2}" = "x${IP_GATEWAY}" -a "x${3}" = "x${IP_NETMASK}" ];
    then
        return 0
    fi

    return 1
}

ntp_servers_has_changed() {
    if [ "x${1}" = "x${NTPSERVERS}" ];
    then
        return 0
    fi

    return 1
}

create_settings_file() {
    IP_ADDR=$1
    IP_GATEWAY=$2
    IP_NETMASK=$3

    echo "IP_ADDR=${IP_ADDR}" >${SETTINGS_FILE}
    echo "IP_NETMASK=${IP_NETMASK}" >>${SETTINGS_FILE}
    echo "IP_GATEWAY=${IP_GATEWAY}" >>${SETTINGS_FILE}
}

restart_eth0() {
    # restart network
    if [ -f "/lib/systemd/system/networking.service" ];
    then
       systemctl restart networking
    else
        ifdown eth0
        ifup eth0
    fi
}

create_interfaces_file() {
    mv $INTERFACES_FILE $INTERFACES_BACKUP
    sed "
s/%GENERATE_APP%/${APP_NAME}/g
s/%GENERATED_DATE%/`date`/g
s/%ETH0_TYPE%/static/g
s/%ETH0_ADDRESS%/${IP_ADDR}/g
s/%ETH0_NETMASK%/${IP_NETMASK}/g
s/%ETH0_GATEWAY%/${IP_GATEWAY}/g
" < $TEMPLATE_FILE >$INTERFACES_FILE

    if [ "x${1}" = "x" ];
    then
        restart_eth0
    fi
}

expert_mode() {
    if [ "x${1}" = "x" -o "x${2}" = "x" -o "x${3}" = "x" ]
    then
        echo Not enough arguments, aborting...
        return 0
    fi

    # echo $1 $2 $3
    # return 1

    ipa=$1
    ipgw=$2
    netmask=$3
    create_settings_file $ipa $ipgw $netmask
    create_interfaces_file no-restart
}

restore_factory_settings() {
    rm -f $INTERFACES_BACKUP
    cp -p $INTERFACES_FACTORY $INTERFACES_FILE

    restart_eth0
}

restore_previous_settings() {
    mv $INTERFACES_BACKUP $INTERFACES_FILE

    restart_eth0
}

get_ntp_servers() {
    servers=$1
    read -p "Enter NTP server(s) separated by spaces (${servers}): " servers
    if [ -z "${servers}" -o "x${servers}" = "xq" -o "x${servers}" = "xQ" ];
    then
        return 0
    fi

    echo "${servers}"
}

get_ip_address() {
    prompt=$1
    ipaddr=$2

    regex="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"

    good=1
    while [ $good -eq 1 ];
    do
        read -p "$prompt (${ipaddr}): " ipaddr
        if [ -z "${ipaddr}" -o "x${ipaddr}" = "xq" -o "x${ipaddr}" = "xQ" ];
        then
            ipaddr=$2
            break
        fi

        foo=$(echo "${ipaddr}" | egrep ${regex})
        if [ "$?" -eq 0 ];
        then
            good=0
        else
            echo "${ipaddr} is not a good IP address, try again or quit" >&2
            ipaddr=$2
        fi
    done

    echo "${ipaddr}"
}

set_ip_address() {
    if show_current_ip_settings;
    then
        read -p "Do you want to change these settings [Y/n]? " yn
        case ${yn} in
            [nN]* )
                return;;
        esac
    fi

    read -p "Should we get IP address from DHCP [y/N]? " yn
    case ${yn} in
        [yY]* )
            restore_factory_settings
            create_settings_file
            ;;
        *)
            ipa=$(get_ip_address "Enter IP address" $IP_ADDR)
            if [ -z "${ipa}" ];
            then
                echo Aborting...
                return
            fi
            netmask=$(get_ip_address "Enter netmask" $IP_NETMASK)
            if [ -z "${netmask}" ];
            then
                echo Aborting...
                return
            fi
            ipgw=$(get_ip_address "Enter gateway IP address" $IP_GATEWAY)
            if [ -z "${ipgw}" ];
            then
                echo Aborting...
                return
            fi

            if [ ! $(ip_address_has_changed $ipa $ipgw $netmask) ];
            then
                create_settings_file $ipa $ipgw $netmask
                create_interfaces_file
            fi
            ;;
    esac
}

configure_ntp_servers() {
    if show_current_ntp_settings;
    then
        read -p "Do you want to change these settings [Y/n]? " yn
        case ${yn} in
            [nN]* )
                return;;
        esac
    fi
    ntp_servers=$(get_ntp_servers "$NTPSERVERS")
    if [ -z "${ntp_servers}" ];
    then
        echo Aborting...
        return
    fi
    if [ ! $(ntp_servers_has_changed $ntp_servers) ];
    then
        echo "Updating ${NTP_SETTINGS_FILE}..."
	cp -p ${NTP_SETTINGS_FILE} "${NTP_SETTINGS_FILE}.bak"
        sed "s/NTPSERVERS=.*/NTPSERVERS=\"${ntp_servers}\"/g" < "${NTP_SETTINGS_FILE}.bak" > ${NTP_SETTINGS_FILE}

        . ${NTP_SETTINGS_FILE}
    fi
}

interactive_session() {
    ans=''
    while [ true ];
    do
        show_menu
        read ans
        case ${ans} in
            "1")
                set_ip_address
                ;;
            "2")
                configure_ntp_servers
                ;;
            [Qq3]* )
                break
                ;;
        esac
    done
}

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    show_help 0
fi

if [ $(id -u) != "0" ]; then
    echo "Must run this script as root!"
    exit 1
fi

if [ -f $SETTINGS_FILE ];
then
    . $SETTINGS_FILE
fi

if [ -f $NTP_SETTINGS_FILE ];
then
    . $NTP_SETTINGS_FILE
fi

if [ $# -ge 1 ]; then
    case $1 in
        -[gG]|--generate )
            create_interfaces_file
            ;;
        -[rR]|--restore )
            restore_previous_settings
            ;;
        -[fF]|--factory )
            restore_factory_settings
            ;;
        -[iI]|--interactive )
            interactive_session
            ;;
        -[eE]|--expert )
            if [ $# -ne 4 ]; then
                echo "ERROR: wrong argument count"
                show_help 1
            fi
            shift
            expert_mode $*
            ;;
        -[sI]|--show )
            echo Ethernet configuration: >&2
            ip address show eth0 >&2
            echo
            echo NTP configuration: >&2
            cat ${NTP_SETTINGS_FILE} >&2
            ;;
        *)
            show_help 1
            ;;
    esac
else
    interactive_session
fi

exit 0
